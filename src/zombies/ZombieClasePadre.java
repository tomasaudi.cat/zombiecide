package zombies;

import humanos.Humanoide;

public abstract class ZombieClasePadre extends Humanoide {
	protected int movimientoZombie;
	protected int damage;
	protected String tipoZombie;

	public ZombieClasePadre(String nombre, int saludMaxima,int salud, boolean vivo,int movimiento, int damage,String tipo) {
		super(nombre, saludMaxima,salud,vivo);	
		setMovimientoZombie(movimiento);
		setDamage(damage);
		setTipoZombie(tipo);
	}
	private void setDamage(int damage2) {
		damage=	damage2;
	}
	public int getMovimientoZombie() {
		return movimientoZombie;
	}

	public void setMovimientoZombie(int movimiento) {
		movimientoZombie = movimiento;
	}
	
	public int getDamageZombie() {
		return damage;
	}

	public void setTipoZombie (String tipoZ) {
		tipoZombie= tipoZ;
	}
	public String getTipoZombie() {
		return tipoZombie;
	}


	// Método toString para representación textual del objeto
	@Override
	public String toString() {
		return "Zombie{" + "movimiento=" + movimientoZombie + ", daño=" + damage + ", tipo='" + tipoZombie + '\''
				+ '}';

	}
}