package humanos;

public abstract class Humanoide {

	protected String nombre;

	protected int salud;

	protected int saludMaxima;

	protected boolean vivo;

	public Humanoide(String nombre, int saludMaxima, int salud, boolean vivo) {

		setNombre(nombre);

		setSaludMaxima(saludMaxima);

		setSalud(salud); // Al iniciar, la salud es la máxima

		setVivo(vivo); // Al iniciar, está vivo

	}

	// Método para recibir daño

	public void recibirDamage(int damageRecibido) {

		salud -= damageRecibido;

		if (salud <= 0) {

			salud = 0;

			vivo = false;

		}

	}

	// Método para curarse

	public void curarse(int cantidad) {

		if (vivo) {

			salud += cantidad;

			if (salud > saludMaxima) {

				salud = saludMaxima;

			}

		}

	}

	// Método para verificar si está vivo

	public  boolean getVivo() {

		return vivo;

	}

	public void setVivo(boolean vivo) {

		this.vivo = vivo;
		vivo = true;
	}

	// Método para obtener el nombre

	public String getNombre() {

		return nombre;

	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	// Método para obtener la salud

	public int getSalud() {

		return salud;

	}

	public void setSalud(int salud) {
		this.salud = salud;
	}

	// Método para obtener la salud máxima

	public int getSaludMaxima() {

		return saludMaxima;

	}

	public void setSaludMaxima(int saludmaxima) {
		saludMaxima = saludmaxima;
	}

}
