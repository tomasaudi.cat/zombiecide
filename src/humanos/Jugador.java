package humanos;
	public class Jugador extends Humanoide {

	    private String arma;

	    private boolean haPasado;

	    public Jugador(String nombre,int salud, int saludMaxima,boolean vivo, String arma, boolean haPasado) {

	        super(nombre, salud, saludMaxima, vivo); // Al iniciar, el jugador está vivo con salud máxima

	        setArma(arma);

	        setHaPasado(haPasado); // Al iniciar, el jugador no ha pasado

	    }

	    // Método para obtener el arma del jugador

	    public String getArma() {

	        return arma;
	    }
	    
	    // Método para establecer el arma del jugador

	    public void setArma(String arma) {

	        this.arma = arma;
	    }
	    
	    // Método para verificar si el jugador ha pasado en la ronda

	    public boolean haPasado() {

	        return haPasado;
	    }
	    
	    // Método para marcar que el jugador ha pasado en la ronda

	    public void pasar() {

	        setHaPasado(true);
	    }

	    // Método para reiniciar el estado de "ha pasado" del jugador

	    public void reiniciarPasar() {

	        setHaPasado(false);

	    }

	    // Setter para haPasado

	    public void setHaPasado(boolean haPasado) {

	        this.haPasado = haPasado;

	    }

	}

