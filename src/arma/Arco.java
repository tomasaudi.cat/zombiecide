package arma;

public class Arco extends ArmaClasePadre{

	public Arco(String nombre, int damage, int alcance, int acierto) {
		super(nombre, damage, alcance, acierto);
	}
	
	@Override
	public void ataqueEspecial() {
		System.out.println("Vaya! Este ataque especial "
				+ "te ha otorgado 'Mata gratis a 1 corredor.'");
	}
}

