package arma;

public class Hechizo extends ArmaClasePadre{

	public Hechizo(String nombre, int damage, int alcance, int acierto) {
		super(nombre, damage, alcance, acierto);
	}
	
	@Override
	public void ataqueEspecial() {
		System.out.println("Vaya! Este ataque especial "
				+ "te ha otorgado 'Mata gratis a 2 caminantes.'");
	}
}

