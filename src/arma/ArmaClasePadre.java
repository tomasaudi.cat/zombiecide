package arma;

public class ArmaClasePadre {

	// ATRIBUTOS
	private String nombre;
	private int damage;
	private int alcance;
	private int acierto;

	// CONSTRUCTOR
	public ArmaClasePadre(String nombre, int damageArma, int alcance, int acierto) {

		setNombre(nombre);
		setDamage(damageArma);
		setAlcance(alcance);
		setAcierto(acierto);
	}

	// ATAQUE ESPECIAL (NO HACE NADA)
	public void ataqueEspecial() {
		System.out.println("Vaya! Este ataque especial no hace nada.");
	}

	// TO-STRING
	@Override
	public String toString() {
		return "Arma{" + "Nombre: " + nombre + ", Daño: " + damage + ", Alcance: " + alcance + ", Alcance: " + "}";
	}

	// GETTERS & SETTERS

	// getNombre & setNombre
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	// getDamage & setDamage
	public int getDamage() {
		return damage;
	}

	public void setDamage(int damageArma) {
		damage = damageArma;
	}

	// getAlcance & setAlcance
	public int getAlcance() {
		return alcance;
	}

	public void setAlcance(int alcance) {
		this.alcance = alcance;
	}

	// getAcierto && setAcierto
	public int getAcierto() {
		return acierto;
	}

	public void setAcierto(int acierto) {
		this.acierto = acierto;
	}

}

// MÉTODO FUNCIONAMIENTO DE LAS ARMAS
