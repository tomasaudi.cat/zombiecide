package zombicide;

public class general {

	    private String nombre;
	    private int salud;
	    private int saludMaxima;
	    private boolean vivo;

	    // Constructor
	    public void Humanoide(String nombre, int saludMaxima) {
	        this.nombre = nombre;
	        this.saludMaxima = saludMaxima;
	        this.salud = saludMaxima; // Al inicio, la salud es la máxima
	        this.vivo = true; // Al inicio, el humanoide está vivo
	    }

	    // Métodos para acceder y modificar los atributos
	    public String getNombre() {
	        return nombre;
	    }

	    public int getSalud() {
	        return salud;
	    }

	    public int getSaludMaxima() {
	        return saludMaxima;
	    }

	    public boolean isVivo() {
	        return vivo;
	    }

	    // Método para recibir daño
	    public void recibirDanio(int danio) {
	        salud -= danio;
	        if (salud <= 0) {
	            salud = 0;
	            vivo = false;
	            System.out.println(nombre + " ha sido derrotado.");
	        }
	    }

	    // Método para curarse
	    public void curarse(int cantidad) {
	        if (vivo) {
	            salud += cantidad;
	            if (salud > saludMaxima) {
	                salud = saludMaxima;
	            }
	            System.out.println(nombre + " se ha curado. Salud actual: " + salud);
	        } else {
	            System.out.println(nombre + " no puede curarse porque está muerto.");
	        }
	    }

	    // Método para revivir (usado en caso de necesidad, por ejemplo, en el caso de jugadores)
	    public void revivir() {
	        if (!vivo) {
	            vivo = true;
	            salud = saludMaxima; // Restauramos la salud máxima al revivir
	            System.out.println(nombre + " ha sido revivido con salud completa.");
	        } else {
	            System.out.println(nombre + " ya está vivo.");
	        }
	    }
}
